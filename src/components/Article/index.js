import React, { PureComponent } from 'react';
import './article.scss'

class Article extends PureComponent {
    constructor(props) {
        super(props)
        this.state = {
            count: 0
        }
    }


    renderTagsList(tags) {
        return tags
    }
    render() {
        const { article, isOpen, onButtonClick } = this.props
        // console.log('----', this.props)
        const body = isOpen && <section className="card-text">
            <ul className="card-info-list">
                <li>
                    Comments: <span> {article.comments} </span>
                </li>
                <li>
                    Likes: <span> {article.likes} </span>
                </li>
                <li>
                    Tags: <span> {article.tags} </span>
                </li>
            </ul>

        </section>
        return (
            <div className="card mx-auto">
                <div className="card-header">
                    <div className="card-header__image">
                        <img src={article.largeImageURL} />
                    </div>

                    <h2>
                        <button onClick={onButtonClick} className="btn btn-primary btn-lg float-right">
                            {isOpen ? 'close' : 'open'}
                        </button>
                    </h2>
                </div>
                <div className="card-body">
                    {body}
                </div>
            </div>
        )
    }
    incrementCounter = () => {
        this.setState({
            count: this.state.count + 1
        })
    }


}

export default Article