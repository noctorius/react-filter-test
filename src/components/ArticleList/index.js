import React, { PureComponent } from 'react'
import Article from '../Article'
import './style.scss'
import {connect} from 'react-redux'
import debounce from 'debounce'
import axios from 'axios'
class ArticleList extends PureComponent {
    state = {
        openArticleId: null,
        tag: '',
        localImages: []
    }
    onChangeTag (event) {
        this.setState({tag: event.target.value})
    }
    async loadImages () {
        const response = await axios.get('https://pixabay.com/api/?key=13510630-ea794ea7bcad1375de70fadeb', {
            params: {
                q: 'cats',
                image_type: 'all',
                per_page:100
            }
        })
        this.setState({ localImages: response.data.hits})
        console.info(response.data.hits)
        // TODO: add to store
    }
    render() {
        const images = this.props.images.filter(image => {
            return image.tags.indexOf(this.state.tag) > -1
        })
        const articleElements = this.state.localImages.map((article, index) =>
            <li key={article.id} className="article-list__li">
                <Article article={article} 
                    isOpen={this.state.openArticleId === article.id}  
                    onButtonClick={this.handleClick.bind(this, article.id) }
                />

            </li>
        )

        return (
            <div>
                <label>
                    <button onClick={this.loadImages.bind(this)}>Load</button>
                    <input onChange={this.onChangeTag.bind(this)} value={this.state.tag} />
                </label>
                <ul className="article-list">
                    {articleElements}
                </ul>
            </div>
        )
    }

    handleClick = openArticleId => this.setState({ 
        openArticleId: this.state.openArticleId === openArticleId ? null : openArticleId
    })
}


export default connect(
    state => ({
        images: state.images
    }),
    dispatch => ({
        // toggleTodo: id => dispatch(toggleTodo(id))
    })

)(ArticleList)