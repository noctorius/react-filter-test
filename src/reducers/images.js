const images = (state = [{
    "largeImageURL": "https://pixabay.com/get/54e0dd404e5bae14f6da8c7dda79367d103edee350516c48702973d7974fc559b1_1280.jpg",
    "webformatHeight": 390,
    "webformatWidth": 640,
    "likes": 1205,
    "imageWidth": 4928,
    "id": 2083492,
    "user_id": 1777190,
    "views": 362555,
    "comments": 141,
    "pageURL": "https://pixabay.com/photos/cat-young-animal-curious-wildcat-2083492/",
    "imageHeight": 3008,
    "webformatURL": "https://pixabay.com/get/54e0dd404e5bae14f6da8c7dda79367d103edee350516c48702973d7974fc559b1_640.jpg",
    "type": "photo",
    "previewHeight": 91,
    "tags": "cat, young animal, curious",
    "downloads": 191855,
    "user": "susannp4",
    "favorites": 1098,
    "imageSize": 4130948,
    "previewWidth": 150,
    "userImageURL": "https://cdn.pixabay.com/user/2015/12/16/17-56-55-832_250x250.jpg",
    "previewURL": "https://cdn.pixabay.com/photo/2017/02/20/18/03/cat-2083492_150.jpg"
},
    {
        "largeImageURL": "https://pixabay.com/get/57e2dd464c51a814f6da8c7dda79367d103edee350516c48702973d7974fc559b1_1280.png",
        "webformatHeight": 402,
        "webformatWidth": 640,
        "likes": 851,
        "imageWidth": 3677,
        "id": 1285634,
        "user_id": 127419,
        "views": 384558,
        "comments": 98,
        "pageURL": "https://pixabay.com/photos/cat-animal-cat-s-eyes-eyes-pet-1285634/",
        "imageHeight": 2310,
        "webformatURL": "https://pixabay.com/get/57e2dd464c51a814f6da8c7dda79367d103edee350516c48702973d7974fc559b1_640.png",
        "type": "photo",
        "previewHeight": 94,
        "tags": "cat, animal, cat's eyes",
        "downloads": 103065,
        "user": "cocoparisienne",
        "favorites": 852,
        "imageSize": 11720209,
        "previewWidth": 150,
        "userImageURL": "https://cdn.pixabay.com/user/2019/09/03/04-54-12-557_250x250.jpg",
        "previewURL": "https://cdn.pixabay.com/photo/2016/03/28/12/35/cat-1285634_150.png"
    },
    {
        "largeImageURL": "https://pixabay.com/get/57e5d54b4c53af14f6da8c7dda79367d103edee350516c48702973d7974fc559b1_1280.jpg",
        "webformatHeight": 472,
        "webformatWidth": 640,
        "likes": 619,
        "imageWidth": 2894,
        "id": 1508613,
        "user_id": 127419,
        "views": 142881,
        "comments": 83,
        "pageURL": "https://pixabay.com/photos/cat-animal-cat-portrait-cat-s-eyes-1508613/",
        "imageHeight": 2135,
        "webformatURL": "https://pixabay.com/get/57e5d54b4c53af14f6da8c7dda79367d103edee350516c48702973d7974fc559b1_640.jpg",
        "type": "photo",
        "previewHeight": 110,
        "tags": "cat, animal, cat portrait",
        "downloads": 70273,
        "user": "cocoparisienne",
        "favorites": 603,
        "imageSize": 1630104,
        "previewWidth": 150,
        "userImageURL": "https://cdn.pixabay.com/user/2019/09/03/04-54-12-557_250x250.jpg",
        "previewURL": "https://cdn.pixabay.com/photo/2016/07/10/21/47/cat-1508613_150.jpg"
    }], action) => {
    switch (action.type) {
        case 'ADD_IMAGE':
            return [
                ...state,
                {
                    id: action.id,
                    largeImageURL: action.largeImageURL,
                    previewURL: action.previewURL,
                    likes: action.likes,
                    comments: action.comments,
                    tags: action.tags.split(', '), // Convert to array
                }
            ]
        default:
            return state
    }
}

export default images